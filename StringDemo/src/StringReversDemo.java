
public class StringReversDemo {
	public static void main(String[] args) {

		String name = "jaffna";
		byte[] nameByte = name.getBytes();
		System.out.println(new String(nameByte));

		byte[] revByte = new byte[nameByte.length];

		for (int i = nameByte.length - 1; i >= 0; i--) {
			revByte[nameByte.length - 1 - i] = nameByte[i];
		}
		System.out.print(new String(revByte));

	}
}
