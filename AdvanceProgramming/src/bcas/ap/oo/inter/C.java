package bcas.ap.oo.inter;

public class C implements InterfaceA, InterfaceB {

	@Override
	public String methodB() {

		return B + A;
	}

	@Override
	public String methodA() {

		return A + B;
	}

}
