package bcas.ap.SupCon;

public class Country {
	String name;
	int area;
	String flag;

	public Country(String name, int area, String flag) {
		this.name = name;
		this.area = area;
		this.flag = flag;

	}

	public void print() {

		System.out.println("name:" + name);
		System.out.println("area:" + area);
		System.out.println("flag:" + flag);
	}
}
