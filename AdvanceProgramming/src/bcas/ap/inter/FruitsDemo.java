package bcas.ap.inter;

public class FruitsDemo {
	public static void main(String[] args) {
		Apple apple = new Apple();
		Mango mango = new Mango();

		apple.printfruitName();
		System.out.println(apple.tellshape());
		System.out.println(apple.tellcolour());

		mango.printfruitName();
		System.out.println(mango.tellshape());
		System.out.println(mango.tellcolour());

		String[] MadeIn = mango.MadeIn();
		for (String made : MadeIn) {
			System.out.print(made + ",");

		}
	}
}
