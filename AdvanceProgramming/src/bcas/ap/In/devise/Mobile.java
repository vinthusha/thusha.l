package bcas.ap.In.devise;

public class Mobile {
	String os;
	String brand;
	String shape;

	public Mobile(String os, String brand, String shape) {
		this.os = os;
		this.brand = brand;
		this.shape = shape;

	}

	public void print() {
		System.out.println("os:" + os);
		System.out.println("brand:" + brand);
		System.out.println("shape:" + shape);
	}
}
