package bcas.ap.In.Inheri;

public class Xchild extends Xparent {
	public void callparentMethods() {
		super.displayNum();
		System.out.println("Xparent age is :" + super.age);
	}

	public static void main(String[] args) {
		Xchild cXchild = new Xchild();
		cXchild.callparentMethods();
	}

}
