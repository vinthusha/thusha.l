package bcas.ap.In.Inheri;

public class Parent implements IParent {

	@Override
	public int add(int num1, int num2) {

		return num1 + num2;
	}

	@Override
	public int sub(int num1, int num2) {

		return num1 - num2;
	}

}
