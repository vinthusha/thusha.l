package condition1;

import java.util.Scanner;

public class MathamticalOp {
	
public double add(double num1, double num2) {
		return num1 + num2;
	}

	public double sub(double num1, double num2) {
		return num1 - num2;
	}

	public double mul(double num1, double num2) {
		return num1 * num2;
	}

	public double div(double num1, double num2) {
		return num1 / num2;
	}
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the number 1 : ");
		double num1 = scan.nextDouble();

		System.out.print("Enter the number 2 : ");
		double num2 = scan.nextDouble();

		System.out.println("Enter the Operataion \n"
				+ "A - Addition\n"
				+ "S - Substration\n" 
				+ "M - Multiplication\n"
				+ "D - Division");
		String op = scan.next();
		
		MathamticalOp mo = new MathamticalOp();
		switch (op) {
		case "A":
			System.out.println(mo.add(num1, num2));
			break;
		case "S":
			System.out.println(mo.sub(num1, num2));
			break;
		case "M":
			System.out.println(mo.mul(num1, num2));
			break;
		case "D":
			System.out.println(mo.div(num1, num2));
			break;
		default:
			System.out.println("Invalied operation !!!");
		}
	}
}

