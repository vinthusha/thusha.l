package project2;

import java.util.Scanner;

public class Digits {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number :  ");
		int n = sc.nextInt();

		if (n <= 9) {
			System.out.println("One Digital");
		} else if (n <= 99) {
			System.out.println("Two Digital");
		} else if (n <= 999) {
			System.out.println("Three Digital");
		} else if (n <= 9999) {
			System.out.println("Four Digital");
		} else if (n > 10000) {
			System.out.println("Invaild Digital");

		}
	}

}
