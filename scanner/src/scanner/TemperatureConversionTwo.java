package scanner;
import java. util.Scanner;

public class TemperatureConversionTwo {
	public static void main (String args[]) { 
		double cel , far;
		Scanner sc = new Scanner(System.in);
		System.out.println("the temperature in F  :");
		far = sc.nextDouble();
		cel = (far - 32) * 5/9;
		System.out.println("Temperature in C is " + cel);
	}

}
