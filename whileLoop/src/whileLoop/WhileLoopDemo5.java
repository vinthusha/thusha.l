package whileLoop;

import java.util.Scanner;

public class WhileLoopDemo5 {
	public static void main (String[]args) {
		Scanner sc =new Scanner (System.in);
		System.out.println("enter the no of rows:");
		int row =sc.nextInt();
		
		int i =1;
		while(i<=row) {
			int j =1; int k = 1;
			while(j<row-i+1) {
				System.out.print("1");
				j++;
			}
			while(k<=i) {
				System.out.print(i + "");
				k++;
			}
			i++;
			System.out.println();
		}
		
	}

}
