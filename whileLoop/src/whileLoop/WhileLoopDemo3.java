package whileLoop;

public class WhileLoopDemo3 {
	public static void main(String[]args) {
		for(int k=7; k>=1; k--) {
			for(int i=1; i<=k; i++) {
				System.out.print(i);
			}
			System.out.println("");
		}
		for(int k=2; k<=7; k++) {
			for(int i=1; i<=k; i++) {
				System.out.print(i);
			}
			System.out.println("");
		}
	}

}
