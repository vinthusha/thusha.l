import java.util.Scanner;

public class BankAccount {
	public int accBal;

	public static class MyBanking {
		public double accBal = 0.0;

		public void deposit(double depAmt) {
			accBal = accBal + depAmt;
		}

		public void widthdraw(double widAmt) {
			accBal = accBal - widAmt;
		}

		public static void main(String[] args) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Welcome to Ceylon Banking System");
			int n = scan.nextInt();

			BankAccount Bank = new BankAccount();
			int num = 0;

			do {
				System.out.println(" Enter your current balance");
				num = scan.nextInt();
				if (num < 0) {
					System.out.println(" ***Beginning balance must be at least zero,please re-enter.***");
				}
			} while (num < 0);
			Bank.accBal = num;

			do {
				System.out.println(" Enter the number of deposits(0-5) :");
				num = scan.nextInt();
				if (num > 6) {
					System.out
							.println("*** Invalid number of deposits,please re-enter.Enter the number of deposits***");
				}
			} while (num > 0);
			Bank.accBal = num;

			do {
				System.out.println(" Enter the number of withdrawals(0-5:2");
				num = scan.nextInt();
				if (num > 6) {
					System.out.println("*** deposit amount must be greater than zero,please re-enter ***");
				}
			} while (num > 6);
			Bank.accBal = num;

			do {
				System.out.println(" Enter the amount of deposit");
				num = scan.nextInt();
				if (num > 6) {
					System.out.println("***withdrawal amount exceeds current balance,please re-enter ***");
				}
			} while (num < 0);
			Bank.accBal = num;

			do {
				System.out.println(" Enter the amouunt of withdrawal");
				num = scan.nextInt();
				if (num > 6) {
					System.out.println("*** The closing balance is Rs.45");
				}
			} while (num > 0);
			Bank.accBal = num;
		}
	}
}
