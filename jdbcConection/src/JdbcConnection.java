

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConnection {

	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/world";
	static final String USER = "root";
	static final String PASS = "thano";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		try {

			Class.forName(JDBC_DRIVER);

			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			System.out.println("Creating table in given database...");
			stmt = conn.createStatement();
			String sql = "Drop table Employee;";
			stmt.executeUpdate(sql);

			stmt = conn.createStatement();
			String sql1 = "CREATE TABLE EMPLOYEE (id int not NULL, first VARCHAR(255), last VARCHAR(255), age int, PRIMARY KEY ( id ))";
			stmt.executeUpdate(sql1);
			System.out.println("Created table in given database...");
			System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();
			sql1 = "INSERT INTO Employee VALUES (100, 'Kriss', 'Kurian', 18)";
			stmt.executeUpdate(sql1);
			sql1 = "INSERT INTO Employee VALUES (101, 'Enrique', 'John', 25)";
			stmt.executeUpdate(sql1);
			sql1 = "INSERT INTO Employee VALUES (102, 'Taylor', 'Swift', 30)";
			stmt.executeUpdate(sql1);
			sql1 = "INSERT INTO Employee VALUES(103, 'Linkin', 'Park', 28)";
			stmt.executeUpdate(sql1);
			System.out.println("Inserted records into the table...");

			sql1 = "SELECT id, first, last, age FROM Employee";
			ResultSet rs = stmt.executeQuery(sql1);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				int age = rs.getInt("age");
				String first = rs.getString("first");
				String last = rs.getString("last");
				// Display values
				System.out.print("ID: " + id);
				System.out.print(", Age: " + age);
				System.out.print(", First: " + first);
				System.out.println(", Last: " + last);
			}
			rs = stmt.executeQuery("show tables like'Employee'");
			while (rs.next()) {
				System.out.println("EXISTS");
			}

			rs.close();
			stmt.close();
			conn.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			System.out.println("Goodbye!");
		}
	}

	private static Statement Executeupdate(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

}