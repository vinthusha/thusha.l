

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JbdcMarks {

	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/world";
	static final String USER = "root";
	static final String PASS = "thano";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		try {

			Class.forName(JDBC_DRIVER);

			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			System.out.println("Creating table in given database...");
			stmt = conn.createStatement();
			String sql = "Drop table Student;";
			stmt.executeUpdate(sql);

			stmt = conn.createStatement();
			String sql11 = "CREATE TABLE Student (id int not NULL, first VARCHAR(255), last VARCHAR(255), Tamil INT,English INT,Maths INT,Grade INT, PRIMARY KEY ( id ))";
			stmt.executeUpdate(sql11);
			System.out.println("Created table in given database...");
			System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();
			sql11 = "INSERT INTO Student VALUES (01, 'Kamal','Kukan',75,60,90, 5 )";
			stmt.executeUpdate(sql11);
			sql11 = "INSERT INTO Student VALUES (02, 'Enrique', 'John',60,50,75, 8)";
			stmt.executeUpdate(sql11);
			sql11 = "INSERT INTO Student VALUES (03, 'Taylor', 'Swift', 75,75,75, 9)";
			stmt.executeUpdate(sql11);
			sql11 = "INSERT INTO Student VALUES(04, 'Linkin', 'Park',50,70,80, 6)";
			stmt.executeUpdate(sql11);
			System.out.println("Inserted records into the table...");

			String sql1 = "update Student" + " set Tamil = 90 where id = (01)";
			stmt.executeUpdate(sql1);
			System.out.println("update records into the table...");

			sql11 = "SELECT id, first, last, Tamil, English, Maths, Grade FROM Student";
			ResultSet rs = stmt.executeQuery(sql11);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				int Tamil = rs.getInt("Tamil");
				int English = rs.getInt("English");
				int Maths = rs.getInt("Maths");
				String Grade = rs.getString("Grade");
				String first = rs.getString("first");
				String last = rs.getString("last");

				// Display values
				System.out.print(",ID: " + id);
				System.out.print(", Tamil: " + Tamil);
				System.out.print(", English: " + English);
				System.out.print(", Maths: " + Maths);
				System.out.print(", Grade: " + Grade);
				System.out.print(", First: " + first);
				System.out.println(", Last: " + last);

			}
			rs = stmt.executeQuery("show tables like'Student'");
			while (rs.next()) {
				System.out.println("EXISTS");
			}

			rs.close();
			stmt.close();
			conn.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			System.out.println("Goodbye!");
		}
	}

}
