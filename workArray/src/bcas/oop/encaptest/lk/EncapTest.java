package bcas.oop.encaptest.lk;

public class EncapTest {
	private String name;
	private String idNum;
	private int age;

	public int getage() {
		return age;
	}

	public String getName() {
		return name;
	}

	public String getIdNum() {
		return idNum;
	}

	public void setAge(int newAge) {
		age = newAge;
	}

	public void setName(String newName) {
		name = newName;
	}

	public void setIdNum(String string) {
		idNum = string;
	}

	public String getAge() {
		return null;
	}
}
