package bcas.ap.utill;

import java.util.Scanner;

public class readvalues {
	Scanner scan = new Scanner(System.in);

	public int readInt(String msg) {
		int num = 0;
		System.out.println(msg);
		num = scan.nextInt();
		return num;
	}

	public String readString(String msg) {
		String value;
		System.out.println(msg);
		value = scan.nextLine();
		return value;
	}

	public int[] readtwovalue(String msg) {
		int[] twovalue = new int[2];

		String value = readString(msg);
		Scanner s = new Scanner(value).useDelimiter(",");

		twovalue[0] = s.nextInt();
		twovalue[1] = s.nextInt();
		s.close();

		return twovalue;
	}
}
