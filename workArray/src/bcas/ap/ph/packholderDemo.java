package bcas.ap.ph;

import bcas.ap.utill.readvalues;

public class packholderDemo {
	public static void main(String[] args) {

		readvalues rv = new readvalues();
		int[] rowcol = rv.readtwovalue(packmessages.getRowcol);

		packholder ph = new packholder(rowcol[0], rowcol[1]);

		int holeNo = rv.readInt(packmessages.gettoken);
		System.out.println(ph.checkAvailability(holeNo));
	}
}
