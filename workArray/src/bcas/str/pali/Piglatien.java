package bcas.str.pali;

public class Piglatien {
	final static String Piglatien = "PAA";

	public static String createString(String inputStr) {
		return inputStr.substring(1) + inputStr.charAt(0) + Piglatien;
	}

	public static String breakString(String inputStr) {
		if (inputStr.toUpperCase().endsWith(Piglatien)) {
			int trimsize = Piglatien.length() + 1;
			return inputStr.charAt(inputStr.length() - trimsize) + inputStr.substring(0, inputStr.length());
		} else {
			return "Couldnt encript this word";
		}

	}
}
