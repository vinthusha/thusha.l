package bcas.ap.tda;

public class TDOpperation {
	static int total = 0;

	public static int findTotal(int[] marks) {
		int total = marks[0];
		for (int mark : marks) {
			total += mark;
		}
		return total;
	}

	public static double findAve(int[] marks) {
		return findTotal(marks) / Double.parseDouble(marks.length + "");
	}

}
