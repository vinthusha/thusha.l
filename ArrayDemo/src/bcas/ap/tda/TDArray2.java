package bcas.ap.tda;

public class TDArray2 {
	public static void main(String[] args) {
		int[] AMarks = { 75, 90, 100, 78, 100 };
		int[] BMarks = { 100, 95, 90, 80, 95 };
		int[] CMarks = { 95, 100, 90, 80, 70 };
		int[] DMarks = { 90, 90, 75, 80, 70 };
		int[] EMarks = { 80, 85, 80, 75, 70 };

		System.out.println("Total of A marks is :" + TDOpperation.findTotal(AMarks));
		System.out.println("Average of A marks is :" + TDOpperation.findAve(AMarks));
		System.out.println("*-------------------------*");

		System.out.println("Total of B marks is :" + TDOpperation.findTotal(BMarks));
		System.out.println("Average of B marks is :" + TDOpperation.findAve(BMarks));
		System.out.println("*-------------------------*");

		System.out.println("Total of C marks is :" + TDOpperation.findTotal(CMarks));
		System.out.println("Average of C marks is :" + TDOpperation.findAve(CMarks));
		System.out.println("*-------------------------*");

		System.out.println("Total of D marks is :" + TDOpperation.findTotal(DMarks));
		System.out.println("Average of D marks is :" + TDOpperation.findAve(DMarks));
		System.out.println("*-------------------------*");

		System.out.println("Total of E marks is :" + TDOpperation.findTotal(EMarks));
		System.out.println("Average of E marks is :" + TDOpperation.findAve(EMarks));
		System.out.println("*---------------------------*");

	}

}
