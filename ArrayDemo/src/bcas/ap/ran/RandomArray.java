package bcas.ap.ran;

import java.util.Random;

public class RandomArray {
	public static int[] createRandomArray(int size, int range) {
		Random ran = new Random();
		int[] ranArray = new int[size];
		for (int i = 0; i < size; i++) {
			ranArray[i] = ran.nextInt(range);
		}
		printRandomArray(ranArray);
		return ranArray;
	}

	private static void printRandomArray(int[] array) {
		for (int element : array) {
			System.out.println(element + " , ");
		}

	}

	public static int[] createRandomMarks(int size, int range) {
		Random ran = new Random();
		int[] ranArray = new int[size];
		for (int i = 0; i < size; i++) {
			ranArray[i] = ran.nextInt(range);
		}
		return ranArray;
	}

}
