package bacs.td.marks.util;

import java.util.Random;

public class RandomGenerator {
	private int range = 0;

	public RandomGenerator(int range) {
		this.range = range;
	}

	public int geneteRandomnumber() {
		Random rand = new Random();
		return rand.nextInt(range);
	}
}