
public class MarksOpperation {
	static int total = 0;

	public static int findTotal(int[] marks) {
		int total = marks[0];
		for (int mark : marks) {
			total += mark;
		}
		return total;
	}

	public static int findmax(int[] marks) {
		int max = marks[0];
		for (int mark : marks) {
			if (mark > max) {
				max = mark;
			}

		}
		return max;
	}

	public static int findmin(int[] marks) {
		int min = marks[0];
		for (int mark : marks) {
			if (mark < min) {
				min = mark;
			}
		}
		return min;

	}

	public static double findAve(int[] marks) {
		return findTotal(marks) / Double.parseDouble(marks.length + "");
	}

}
