package jdbcDemo;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.SQLException;

public class Driver {

	final static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	final String DB_URL = "jdbc.mysql.// localhost.3306/emp";
	final String USER = "root";
	final String PASS = "thano";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER);
			System.out.println("Connecting to databases....");
			conn.close();
			System.out.println("Connection Success");

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					((Connection) stmt).close();

			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();

			}
			System.out.println("Good bye");
		}

	}

}
